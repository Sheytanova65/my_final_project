package com.telerikacademy.finalproject.pages;

public class NavigationPage extends BasePage {
    public NavigationPage() {
        super("base.url");
    }

    public final String homeButton = "navigation.Home";
    public final String signInButton = "login.Sign.InButton";
    public final String logInButton = "login.LoginButton";
    public final String logOutButton = "logout.LogOutButton";

//
//    @Override
//    public void updatePost(String editPostTitle) {
//
//    }
}
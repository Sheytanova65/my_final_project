package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class AdminPage extends BasePage {
    public AdminPage() {
        super("admin.url");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public void disableUser() {
        actions.clickElement("adminPage.seeProfileButton");
        actions.clickElement("adminPage.disableButton");
    }

    public void enableUser() {
        actions.clickElement("adminPage.seeProfileButton");
        actions.clickElement("adminPage.enableButton");
    }
    public void adminAllPostVisibility(){
        actions.clickElement("adminPage.allPostBrowseButton");
    }
}


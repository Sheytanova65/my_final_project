package com.telerikacademy.finalproject.pages;

public class UsersPage extends BasePage {
    public UsersPage() {
        super("base.url");
    }

    public void searchByName(String name) {
        actions.typeValueInField(name, "search.NameField");
        actions.waitMillis(1000);
        actions.clickElement("search.Button");
        actions.waitMillis(1000);
    }

    public void searchByProfessional(String professional) {
        actions.typeValueInField(professional, "search.ProfessionalField");
        actions.waitMillis(1000);
        actions.clickElement("search.Button");
        actions.waitMillis(1000);
    }

    public void assertProfessionalPresent() {
            actions.waitFor(1000);
            actions.assertElementPresentAfterWait("searched.Professional");
    }

    public void assertNamePresent() {
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("searched.Name");
    }


//    @Override
//    public void updatePost(String editPostTitle) {
//
//    }
}
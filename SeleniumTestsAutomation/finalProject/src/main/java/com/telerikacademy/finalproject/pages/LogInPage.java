package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class LogInPage extends BasePage {
    public LogInPage() {
        super("login.url");
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
    }
        public String usernameUser = Utils.getUIMappingByKey("loginPage.userName");
        public String passwordUser = Utils.getUIMappingByKey("loginPage.password");
        public String invalidPasswordUser = Utils.getUIMappingByKey("loginPage.invalidPassword");


    public void logIn(String username, String password) {
        actions.typeValueInField(username, "loginPage.UsernameField");
        actions.typeValueInField(password, "loginPage.PasswordField");
        actions.clickElement("login.LoginButton");
    }

    public void assertWrongPasswordMessage(String text) {
        Assert.assertTrue("Wrong username or password.", driver.getPageSource().contains(text));
    }
    public void assertLoginSuccessful(){
        actions.assertElementPresentAfterWait("loginPage.Personal");
    }

//    @Override
//    public void updatePost(String editPostTitle) {
//
//    }
}
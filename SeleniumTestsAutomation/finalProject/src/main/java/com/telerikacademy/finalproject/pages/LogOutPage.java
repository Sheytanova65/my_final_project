package com.telerikacademy.finalproject.pages;

public class LogOutPage extends BasePage {
    public LogOutPage() {
        super("logout.url");
    }

    public void logOut() {
        actions.clickElement("logout.LogOutButton");
        actions.waitMillis(1000);
    }

//    @Override
//    public void updatePost(String editPostTitle) {
//
//    }
}

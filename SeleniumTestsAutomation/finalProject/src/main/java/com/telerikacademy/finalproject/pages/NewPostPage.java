package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class NewPostPage extends BasePage {
    public NewPostPage() {
        super("newPost.url");
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }



    public void createNewPublicPost( String message) {
        actions.waitForElementVisible("newPostPage.newPostButton", 50);
        actions.clickElement("newPostPage.newPostButton");
        Select selectBox = new
        Select(driver.findElement(By.id("StringListId")));
        selectBox.selectByVisibleText("Public post");
        actions.waitForElementVisible("newPostPage.massageField", 50);
        actions.typeValueInField(message, "newPostPage.massageField");
        actions.clickElement("newPostPage.save");
        actions.waitMillis(5000);

    }
    public void likePost(){
        actions.waitFor(5000);
        actions.clickElement("newPostPage.likePost");
        actions.waitFor(5000);
    }
    public void dislikePost(){
        actions.waitFor(5000);
        actions.clickElement("newPostPage.dislikePost");
        actions.waitFor(10000);
    }


    public void CreateComment(String comment) {

        actions.moveToElement("newPostPage.ExploreThisPostButton");
        actions.waitFor(5000);
        actions.clickElement("newPostPage.ExploreThisPostButton");
        actions.waitFor(2000);
        actions.clickElement("newPostPage.commentField");
        actions.typeValueInField("good news", "newPostPage.commentField");
        actions.waitFor(3000);
        actions.clickElement("newPostPage.postComment");
        actions.waitFor(3000);
    }

        public void DeleteNewPost() {
            actions.waitFor(10000);
            actions.moveToElement("newPostPage.ExploreThisPostButton");
            actions.isElementPresentUntilTimeout("newPostPage.ExploreThisPostButton",10);
            actions.clickElement("newPostPage.ExploreThisPostButton");
            actions.isElementPresentUntilTimeout("newPostPage.deleteButton",10);
            actions.clickElement("newPostPage.deleteButton");
            actions.isElementPresentUntilTimeout("newPostPage.assertText",10);
            actions.waitFor(3000);
            actions.clickElement("newPostPage.chooseDeleteButton");
            actions.isElementPresentUntilTimeout("newPostPage.submitButton",10);
            actions.clickElement("newPostPage.submitButton");
        }


    public void assertPostCreated() {
        actions.assertElementPresentAfterWait("newPostPage.massage");
    }
    public void assertPublicPostExists() {

        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("newPostPage.publicStatus");
    }
    public void assertCommentCreated() {

        actions.assertElementPresentAfterWait("newPostPage.comment");
    }
    public void assertLikePost() {
        actions.waitFor(1000);
        actions.assertElementPresent("newPostPage.dislikePost");
        actions.waitFor(10000);
    }
    public void assertDislikePost() {
        actions.waitFor(5000);
        actions.assertElementPresentAfterWait("newPostPage.likePost");
        actions.waitFor(10000);
    }


    public void assertPostDeleted(){
        actions.waitFor(1000);
        actions.assertElementPresentAfterWait("newPostPage.deletedSuccessfully");
}


}

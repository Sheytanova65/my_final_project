Meta:
@logIn

Narrative:
As a registered user
I want to log in my account
So that I can use the functionalities of the social network


Scenario: Unsuccessful login with wrong password
Given Click navigation.Home element
When Click login.SignInButton element
Then Type Ema in login.UserNameField field
Then Type aaa123AAA@ in login.PasswordField field
And Click login.LoginButton element
Then Wrong password message



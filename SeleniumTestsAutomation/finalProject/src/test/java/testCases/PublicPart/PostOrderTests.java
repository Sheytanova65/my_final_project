package testCases.PublicPart;

import com.telerikacademy.finalproject.pages.BasePage;
import com.telerikacademy.finalproject.pages.PostsPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import org.junit.Before;
import org.junit.Test;
import testCases.BaseTest;

public class PostOrderTests extends BaseTest {
    private PostsPage postsPage = new PostsPage();


    @Test
    public void  SU_01_003_OpenOllPublicPosts() {
        postsPage.openAllPublicPosts();
        actions.waitMillis(10);
        postsPage.assertAllPublicPosts();
        actions.waitMillis(10);
    }
    @Test
    public void  SU_01_004_ReadPublicPosts() {
        postsPage.ReadPublicPost();
        actions.waitMillis(10);
        postsPage.assertReadPublicPost();
        actions.waitMillis(10);
    }
}


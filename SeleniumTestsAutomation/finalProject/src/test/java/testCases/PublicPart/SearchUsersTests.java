package testCases.PublicPart;

import com.telerikacademy.finalproject.pages.LogOutPage;
import com.telerikacademy.finalproject.pages.PostsPage;
import com.telerikacademy.finalproject.pages.ProfilePage;
import com.telerikacademy.finalproject.pages.UsersPage;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import testCases.BaseTest;

public class SearchUsersTests extends BaseTest {
    private final ProfilePage profilePage = new ProfilePage();
    public String searchByName= Utils.getUIMappingByKey("profilePage.searchedName");
    public String searchByProfessional= Utils.getUIMappingByKey("profilePage.searchedProfessional");
    @Before
    public void preconditions() {
        profilePage.navigateToPage();
    }


    @Test
    public void  SU_01_008_assertNamePresent() {
        profilePage.searchByName(searchByName);
        actions.waitMillis(1000);
        profilePage.assertNamePresent();

    }

    @Test
    public void SU_01_007_assertProfessionalPresent() {
        profilePage.searchByProfessional(searchByProfessional);
        actions.waitMillis(1000);
        profilePage.assertProfessionalPresent();



    }
}

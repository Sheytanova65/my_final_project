package testCases.AdminPart;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class DisableAndEnableUserTests extends BaseTest {
    private static final LogInPage logInPage = new LogInPage();
    private static final LogOutPage logOutPage = new LogOutPage();
    private static final AdminPage adminPage = new AdminPage();
    private static final ProfilePage profilePage = new ProfilePage();
    public String searchByName = Utils.getUIMappingByKey("adminPage.userFirsName");
    public String username = Utils.getUIMappingByKey("adminPage.userName");
    public String userPass = Utils.getUIMappingByKey("adminPage.userPass");
    public String errorMessage = Utils.getUIMappingByKey("loginPage.errorMessage");


    @Before
    public void beforTest() {
        logInPage.navigateToPage();
        logInPage.logIn(Utils.getUIMappingByKey("weAreAdmin.username"),
                Utils.getUIMappingByKey("weAreAdmin.pass"));
        adminPage.navigateToPage();
        adminPage.assertPageNavigated();
    }

    @Test
    public void successfulDisableUser() {
        profilePage.searchByName(searchByName);
        adminPage.disableUser();
        logOutPage.logOut();
        logInPage.logIn(Utils.getUIMappingByKey(username),
                Utils.getUIMappingByKey(userPass));
        logInPage.assertWrongPasswordMessage(errorMessage);
    }

    @Test
    public void successfulEnableUser() {
        profilePage.searchByName(searchByName);
        adminPage.enableUser();
        logOutPage.logOut();
        logInPage.logIn(Utils.getUIMappingByKey(username),
                Utils.getUIMappingByKey(userPass));
        logInPage.assertLoginSuccessful();
    }
}





